-------------------------------------------------------
/**
 * Author:		Kevin Ammann
 * Created:		12.05.2021
 * Project:		Functional Dependency Detection with PL/pgSQL
 * Version:		Initial Version
 * Function:	Purpose: Create all potential permutations with given attributes
				Parameter: Array of attributes 
**/
-------------------------------------------------------
CREATE OR REPLACE FUNCTION get_permutations(IN attribute_list text[])
    RETURNS text[]
    LANGUAGE 'plpgsql'

AS $BODY$
DECLARE	

attr_combination text[];
attr_count integer;
amt_of_deter integer;

deter text; --all deter attr
deter_attr text; --item of deter attr
determinant text[]; --all deter attr array
determinant_length integer;

depend text; --all deter attr
dependent text[]; --all deter attr array
dependent_length integer;

count integer := 1;
level_deter integer := 1;
level_depend integer := 2;

available boolean;

all_permutations text[];
permutation text;

BEGIN

	attr_count := array_length(attribute_list,1);
	amt_of_deter := attr_count-1;
	--RAISE NOTICE 'attr_count: %',attr_count;
	
	
	attr_combination := ARRAY(WITH RECURSIVE t(i) AS (SELECT * FROM unnest(attribute_list::text[])) 
							,cte AS ( SELECT i AS combo, i, 1 AS ct 
										FROM t 
									   UNION ALL 
									  SELECT cte.combo || ','||t.i, t.i, ct + 1 
										FROM cte 
										JOIN t ON t.i > cte.i) 
									  SELECT combo FROM cte ORDER BY ct, combo);
								
	--RAISE NOTICE 'attr_combination: %',attr_combination;
	
	-- Iterate once for each level
	WHILE count < attr_count LOOP
	
		IF count <= amt_of_deter THEN
			
			--Iterate and find determiner of given level
			FOREACH deter IN ARRAY attr_combination LOOP			
				
				determinant := string_to_array(deter,',');
				--RAISE NOTICE 'determinant: %',determinant;
						
				determinant_length := array_length(determinant,1);
				--RAISE NOTICE 'determinant_length: %',determinant_length;
				
				--Stop iteration when deter klevel is reached
				EXIT WHEN determinant_length > level_deter;	
				
				--Start at proper level of deter
				IF determinant_length = level_deter THEN
				
				    --Iterate again and find dependent of given level
					FOREACH depend IN  ARRAY attr_combination LOOP
						
						dependent := string_to_array(depend,',');
						--RAISE NOTICE 'dependent: %',dependent;
						
						dependent_length := array_length(dependent,1);
						--RAISE NOTICE 'dependent_length: %',dependent_length;
				
						--Stop iteration when deter klevel is reached
						EXIT WHEN dependent_length > level_depend;	
				
						--Start at proper level of depend
						IF dependent_length = level_depend THEN
							
							available := true;
							
							-- Check if all deter in depend
							-- 1,3 --> 1,2,3
							FOREACH deter_attr IN ARRAY determinant LOOP
								IF NOT (deter_attr = ANY(dependent)) THEN
									available := false;								
									--RAISE NOTICE 'available is % since % is not in %',available,deter2,dependent;
								ELSE 
									dependent := array_remove(dependent,deter_attr::text);								
								END IF; 
							END LOOP; -- loop through deter
						
							--If determiner is available in dependent
							IF available THEN

								--RAISE NOTICE '% -> %',array_to_string(determinant,','),array_to_string(dependent,',') ;
								permutation := array_to_string(determinant,',') ||'->' || array_to_string(dependent,',');
								all_permutations = array_append(all_permutations,permutation);
								
							END IF; --deter is available 						
						END IF; --depend_length = level_depend
					END LOOP; -- LOOP_depend			
				END IF;	-- deter_lengt = level_deter	
			END LOOP; -- LOOP_deter
		END IF; --count < amt_of_deter
		count := count + 1;
		-- ensure that deter and depend have always one level difference
		-- one more then deter e.g. 1 -> 2,1 results in 1->2
		level_deter := level_deter + 1;
		level_depend := level_depend + 1;

	END LOOP; -- END LOOP WHILE
	
	--RAISE NOTICE 'get_comb_of_attr_from_pg_stat_statments: %',all_permutations;
	RETURN all_permutations;
END;
$BODY$;


select get_permutations('{player_id,first_name,last_name}');
select get_permutations('{"player_id","first_name","last_name"}');
