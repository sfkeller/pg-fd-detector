# Cloud Database Optimization by the Example of a PostgreSQL Service
## Functional Dependency Detection with PL/pgSQL on a PostgreSQL Cloud Database
Functional dependencies (FDs) are essential for table decomposition, database normalization, and data cleansing. A functional dependency (FD) expresses a value constraint between two sets of attributes.
Functional dependencies between attributes have a strong influence on the estimation accuracy of the query planner. Since the query planner does not know (automatically) that functional dependencies exist, he assumes that the conditions are independent of one another. Accordingly, the size of the result is often underestimated. To prevent this underestimation statistics objects must be created for the corresponding attributes.
(The PostgreSQL Global Development Group, 2016) mentions that it would be too timeconsuming to assess the degree of dependency between all column groups. 
The pg_fd_detector tool provides an easy way to uncover potential functional dependencies in a short time.

## Purpose
Simple PL/pgSQL extension used to find potential functional dependencies.  

## pg_fd_detector 
Instead of considering all attribute of a given table as the search space of functional dependencies, the pg_fd_detector focuses only on attributes that can be found in executed queries that have exceeded a certain execution time. The executed queries are made available by pg_stats_statment, a module provided by PostgreSQL, that stores all queries that have been executed on the server.

The tool essentially consists of the three functions get_attributes, get_permutations and get_functional_dependencies. 

**get_attributes**  
For each stored query that exceeds the execution time specified by the user, the function extracts the table name and the attributes which can be found after the WHERE condition.


**get_permutations**  

All possible permutations of the attributes that were previously extracted from the table pg_stat_statments are found with the help of an algorithm and overgiven to the main function. They serve as potential candidates that need to be examined for functional dependencies in the next step.

**get_functional_dependencies**  
Main function that measures the degree of functional dependency for each permutation received from get_permuations. The strength of functional dependency is measured with a value between 0 and 1, where 0 means “functional independent” and 1 stands for “functional dependent”. Primary key attributes are not examined due to their uniqueness and therefore deterministic nature.

## Installation
* Copy all three functions and create them in your own database. 


## Usage
The following three arguments can optionally be assigned to the function:

*  **min_exec**  
_Descr:_ Minimum time spent executing the statement (in milliseconds). Only queries which have an min_exec_time equal or higher (>=) than the given value are examined.         
_Datatype:_ numeric   
_Range:_    numbber >= 0  
_Default:_  0.5 

*  **coef**  
_Descr:_    The degree of functional dependency. Only permutations which have a coefficient equal or higher (>=) than the given value are listed in the output.        
_Datatype:_ numeric   
_Range:_    0 >= number <= 1  
_Default:_  0.5 

*  **create**  
_Descr:_    If the "create" parameter is given, statistic objects are generated for the dependencies found.  
_Datatype:_ text  
_Range:_    “create”  
_Default:_  NULL (Statistics are not created automatically)

Example of a function call with all parameters:  
        
    SELECT get_functional_dependencies(min_exec=>10, coef=>0.7, create_stats=>'create');  
      
    
Corresponding return value:

    pg_stat_statements query_id:  
      -1566332825180847002  
    Table:  
      atp_players  
    Attributes:  
      player_id,last_name  

    Functional Dependencies:  
      last_name=>player_id: 0.79  
    Statistics created for:
      last_name,player_id


## Author
------
Tool is written by [Kevin Ammann](https://gitlab.com/kevinost).


Feedback is welcome.
 
